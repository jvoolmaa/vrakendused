<?php

$cart_items = array();

$id   = isset($_GET['id']) ? $_GET['id'] : "";
$name = isset($_GET['name']) ? $_GET['name'] : "";

$cart_items[$id]  = $name;
$cookie           = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "";
$cookie           = stripslashes($cookie);
$saved_cart_items = json_decode($cookie, true);

// prevent error, if no items
if (!$saved_cart_items) {
    $saved_cart_items = array();
}


if (array_key_exists($id, $saved_cart_items)) {
    header('Location: products.php?action=exists&id' . $id . '&name=' . $name);
}

else {
    if (count($saved_cart_items) > 0) {
        foreach ($saved_cart_items as $key => $value) {
            $cart_items[$key] = $value;
        }
    }
    
    $json = json_encode($cart_items, true);
    setcookie('cart_items_cookie', $json);
    
    header('Location: products.php?action=added&id=' . $id . '&name=' . $name);
}

?>

