﻿<?php
require_once "database.php";
if (!logged()) {
    register($_POST['username'], $_POST['password'],  $_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['address']);
    login($_POST['username'], $_POST['password']);
    header("Location: products.php");
} else {
    header("Location: registreeri_vaade.php?teade2=Olete juba sisse logitud");
}
?>
