﻿<?php
require("database.php");
$username = $_POST['username'];
$password = $_POST['password'];
if (login($username, $password)) {
    if (isAdmin($_SESSION['id'])) {
        header("Location: admin_vaade.php");
    } else {
        header("Location: products.php");
    }
    
} else {
    header("Location: registreeri_vaade.php?teade=Vale kasutajanimi või parool");
}
?>