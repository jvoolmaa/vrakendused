<?php
function getCon()
{
    $con = mysqli_connect("localhost", "test", "t3st3r123", "test");
    if ($con->connect_error) {
        die("Connection failed: " . $con->connect_error);
    } else {
        return $con;
    }
}

function register($username, $password, $firstname, $lastname, $email, $address)
{
    $con   = getCon();
    //hash the pass and insert into db
    //$hash = password_hash($password, PASSWORD_BCRYPT);
    $sql   = "INSERT INTO jvoolmaa_kasutajad (username, password, firstname, lastname, email, roll, address) VALUES (?, ?, ?, ?, ?, 'user', ?);";
    $query = $con->prepare($sql);
    $query->bind_param('ssssss', $username, $password, $firstname, $lastname, $email, $address);
    $query->execute();
    $con->close();
}
function checkPass($password, $passwordconf)
{
    if (($password == $passwordconf) && !(strlen($password < 8))) {
        return true;
    }
}

function login($username, $password)
{
    startSession();
    $con   = getCon();
    $sql   = "SELECT id, username, password FROM jvoolmaa_kasutajad WHERE username = ? LIMIT 1;";
    $query = $con->prepare($sql);
    $query->bind_param('s', $username);
    $query->execute();
    $result = $query->get_result();
    $row    = $result->fetch_assoc();
    
    $con->close();
    if (isset($row) && $row["password"] == $password) {
        $_SESSION['id']       = preg_replace("/[^0-9]+/", "", $row['id']);
        $_SESSION['username'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["username"]);
        $_SESSION['password'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["password"]);
        return true;
    }
    return false;
}

function logged()
{
    startSession();
    if (isset($_SESSION['id'], $_SESSION['username'], $_SESSION['password'])) {
        $id       = $_SESSION['id'];
        $username = $_SESSION['username'];
        $password = $_SESSION['password'];
        
        $con = getCon();
        
        $sql   = "SELECT password FROM jvoolmaa_kasutajad WHERE id = ? LIMIT 1;";
        $query = $con->prepare($sql);
        $query->bind_param('i', $id);
        $query->execute();
        
        $result = $query->get_result();
        $row    = $result->fetch_assoc();
        if (isset($row)) {
            if ($password == $row['password']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function startSession()
{
    $session_name = 'sess_time'; //sessiaeg
    $secure       = false;
    $httponly     = true;
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name);
    session_start();
    session_regenerate_id(true);
}

function logout()
{
    startSession();
    $_SESSION = array();
    $params   = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    session_destroy();
}
function getProducts()
{
    $con   = getCon();
    $sql   = "SELECT * FROM jvoolmaa_tooted WHERE saadavus=true";
    $query = $con->prepare($sql);
    $query->execute();
    $result = $query->get_result();
    $tooted = array();
    while ($row = $result->fetch_assoc()) {
        $tooted[] = $row;
    }
    return $tooted;
    
    $con->close();
}
function addProduct($nimi, $kirjeldus, $hind, $kogus)
{
    $con   = getCon();
    $sql   = "INSERT INTO jvoolmaa_tooted(nimi, kirjeldus, hind, kogus) VALUES(?,?,?,?)";
    $query = $con->prepare($sql);
    $query->bind_param('ssss', $nimi, $kirjeldus, $hind, $kogus);
    $query->execute();
    $con->close();
}
function isAdmin($id)
{
    $con   = getCon();
    $sql   = "SELECT roll FROM jvoolmaa_kasutajad WHERE id=? AND roll='admin' LIMIT 1;";
    $query = $con->prepare($sql);
    $query->bind_param('i', $id);
    $query->execute();
    
    $result = $query->get_result();
    $row    = $result->fetch_assoc();
    if (isset($row) && $row['roll'] == 'admin') {
        return true;
    } else {
        $con->close();
        return false;
    }
    $con->close();
}
?>