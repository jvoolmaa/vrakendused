<?php
$host = "localhost";
$db_name = "test";
$username = "test";
$password = "t3st3r123";
 
try {
    $con = new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
}
//to handle connection error
catch(PDOException $exception){
    echo "Connection error: " . $exception->getMessage();
}
?>