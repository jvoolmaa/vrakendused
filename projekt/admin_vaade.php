<?php 
   require("database.php");
   include("layout_head.php");
   if(logged()) {
   	echo '<table class="table table-inverse table-responsive table-bordered"><tr>';
               echo "<th class='thead-default'>Toode</th>";
               echo "<th>Kirjeldus</th>";
               echo "<th>Hind (EUR)</th>";
               echo "<th>Kogus</th>";
           echo "</tr>";
   	$tooted = getProducts();
   	foreach($tooted as $toode) {
   		echo '<tr><td>' . $toode['nimi'] . '</td><td>' . $toode['kirjeldus'] . '</td><td>' . $toode['hind'] . '€</td><td>' . $toode['kogus'] . '</td>';
   	}
   	echo '</table>';
       echo '<div class="container"><div class="row"><p><button id="kuva-nupp" class="btn btn-primary">Lisa toode</button></p></div>';
       echo '<div><form id="lisa-vorm" class="form-vertical" action="lisaToode.php" method="POST">';
       echo 
       '<div>
           <div>
               <label for="name">Toode:</label>
               <input type="text" name="nimi" id="name" value="" />
           </div>
           <div>
               <label for="email">Kirjeldus:</label>
               <input type="text" name="kirjeldus" id="email" value="" />
           </div>
           <div>
               <label for="email">Hind:</label>
               <input type="text" name="hind" id="email" value="" />
           </div>
           <div>
               <label for="email">Kogus:</label>
               <input type="text" name="kogus" id="email" value="" />
           </div>
           <button type ="submit" class="btn btn-success">Lisa</button>
       </div>';
       echo '</form></div></div>';
   }
   include("layout_foot.php");
   ?>
