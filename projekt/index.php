﻿<?php require("database.php") ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Estonian Orgonite</title>
      <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="css/index.css">
   </head>
   <body>
      <div>
         <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
               <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Orgonize Estonia</a>
               </div>
               <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav nav-pills navbar-right">
                     <li class="active"><a href="#esimene" data-toggle="tab">Orgoniidist</a></li>
                     <li><a href="#teine" data-toggle="tab">Tooted</a></li>
                     <li><a href="#kolmas" data-toggle="tab">Kinkimine</a></li>
                     <li><a href="registreeri_vaade.php">Pood</a></li>
                  </ul>
               </div>
            </div>
         </nav>
         <div class="jumbotron">
            <div class="container">
               <h1>Orgonize Estonia</h1>
               <h2>Eesti esimene orgoniidi infosait ja veebipood</h2>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-md-7">
                  <a href="#">
                  <img class="img-responsive" src="pildid/orgonite2.jpg" alt="">
                  </a>
               </div>
               <div class="col-md-5">
                  <h2><strong>Orgoniidist</strong></h2>
                  <p class="text-justify">Vähesed on Eestis orgoniidist kunagi midagi kuulnud ning on aeg  seda muuta.</p>
                  <p class="text-justify">Orgoniit on vähetuntud komposiitmaterjal, millel on väidetavalt palju positiivseid mõjusid inimestele, taimedele, loomadele - kõigele elavale. Inimesed on õnnelikumad ja taimed kasvavad kiiremini ja kõrgemale. Orgoniit aitab neutraliseerida elektromagneetilist kiirgust meid ümbritsevast keskkonnast. </p>
               </div>
            </div>
            <hr>
            <div class="row">
               <div class="col-md-5">
                  <h2><strong>Tooted</strong></h2>
                  <h2>Towerbusters</h2>
                  <p class="text-justify">Põhilisteks orgooniiditoodeteks on nn. tower-buster-id, mida maetakse telefonimastide või muude elektromagneetilist reostust tootvate asukohtade lähedale. Need ei näe just liiga elegantsed väljad, kuid teevad oma töö alati ära. TB-sid on nii odav toota, et neid müüakse vahel isegi kilo kaupa.</p>
               </div>
               <div class="col-md-7">
                  <a href="#">
                  <img class="img-responsive" src="pildid/buster.jpg" alt="">
                  </a>
               </div>
            </div>
            <hr>
            <div class="row">
               <div class="col-md-7">
                  <a href="#">
                  <img class="img-responsive" src="pildid/hhg.jpg" alt="">
                  </a>
               </div>
               <div class="col-md-5">
                  <h2><strong>Holy Hand Grenade</strong></h2>
                  <p class="text-justify">
                     Lisaks TB-dele on levinud natuke tugevamad ning rohkem personaalseks kaitseks mõeldud HHG - Holy Hand Grenade. Ühe kristalli asemel on neis 5 ja tihti ka vasest spiraal. Natuke kulukamad ning keerulisemad valmistada, kuid tulemuseks on kvaliteetne energia muundaja, mis näeb ka atraktiivne välja.
                  </p>
               </div>
            </div>
         </div>
         <hr>
         <footer class="text-center">
            <p>&copy; 2016 Orgoniit OÜ.</p>
         </footer>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <script src="js/customjs.js"></script>
   </body>
</html>
