﻿<?php
$page_title = "Ostukorv";
include 'layout_head.php';

$action = isset($_GET['action']) ? $_GET['action'] : "";
$name   = isset($_GET['name']) ? $_GET['name'] : "";


if ($action == 'removed') {
    echo "<div class='alert alert-info'>";
    echo "<strong>{$name}</strong> eemaldati ostukorvist!";
    echo "</div>";
}

$cookie           = $_COOKIE['cart_items_cookie'];
$cookie           = stripslashes($cookie);
$saved_cart_items = json_decode($cookie, true);

if (count($saved_cart_items) > 0) {
    // get the product ids
    $ids = "";
    foreach ($saved_cart_items as $id => $name) {
        $ids = $ids . $id . ",";
    }
    $ids = rtrim($ids, ',');
    
    //tabel
    echo "<table class='table table-inverse table-responsive table-bordered'>";
    
    echo "<tr>";
    echo "<th class='textAlignLeft'>Toote nimi</th>";
    echo "<th>Hind (EUR)</th>";
    echo "<th>Tegevus</th>";
    echo "</tr>";
    
    $sql   = "SELECT id, nimi, hind FROM jvoolmaa_tooted WHERE id IN ({$ids}) ORDER BY nimi";
    $query = $con->prepare($sql);
    $query->execute();
    
    $total_price = 0;
    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        
        echo "<tr>";
        echo "<td>{$nimi}</td>";
        echo "<td>&#128;{$hind}</td>";
        echo "<td>";
        echo "<a href='remove_from_cart.php?id={$id}&name={$nimi}' class='btn btn-danger'>";
        echo "<span class='glyphicon glyphicon-remove'></span> Eemalda korvist";
        echo "</a>";
        echo "</td>";
        echo "</tr>";
        
        $total_price += $hind;
    }
    
    echo "<tr>";
    echo "<td><b>Kokku</b></td>";
    echo "<td>&#128;{$total_price}</td>";
    echo "<td>";
    echo "<a href='#' class='btn btn-success'>";
    echo "<span class='glyphicon glyphicon-shopping-cart'></span> Maksma";
    echo "</a>";
    echo "</td>";
    echo "</tr>";
    
    echo "</table>";
}

else {
    echo "<div class='alert alert-danger'>";
    echo "<strong>Ostukorv on tühi...</strong>";
    echo "</div>";
}

include 'layout_foot.php';
?>