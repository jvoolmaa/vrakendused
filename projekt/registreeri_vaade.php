﻿<?php
   require("database.php");
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Estonian Orgonite</title>
      <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="css/pood.css">
   </head>
   <body>
      <!--/ anchor for top <div id="login"></div> -->
      <nav class="navbar navbar-inverse navbar-fixed-top">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="index.php">Orgonize Estonia</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <form class="navbar-form navbar-right" action="login.php" method="POST">
                  <div class="form-group">
                     <input type="text" placeholder="Kasutajanimi" name="username" class="form-control">
                  </div>
                  <div class="form-group">
                     <input type="password" placeholder="Parool" name="password" class="form-control">
                  </div>
                  <button type="submit" class="btn btn-success">Logi sisse</button>
                  <span style="text-color:white;">
                  <?= isset($_GET['teade'])?$_GET['teade']:"" ?>
                  </span> 
               </form>
            </div>
            <!--/.navbar-collapse -->
         </div>
      </nav>
      <div class="container">
         <div class="jumbotron">
            <div class="container">
               <span>
               <?= isset($_GET['teade2'])?$_GET['teade2']:"" ?>
               </span>
               <div class="text-center">
                  <h1>Eesti esimene orgoniidi veebipood</h1>
                  <h2>Ostmiseks palume eelnevalt registreerida või sisse logida!</h2>
                  <h3><a href="#register"><span class="glyphicon glyphicon-user"></span>Registreeri</a></h3>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="page-header">
            <h1 style="color: white">Eesti esimene orgoniidi veebipood</h1>
         </div>
         <div id="register">
            <form id="regform" class="form-horizontal" action="register.php" method="POST">
               <fieldset>
                  <div id="legend">
                     <legend style="color: white">Registreeri - tärniga märgitud väljad on kohustuslikud</legend>
                  </div>
                  <div class="control-group">
                     <!-- Username -->
                     <label class="control-label"  for="username">Kasutajanimi *</label>
                     <div class="controls">
                        <input type="text" id="username" name="username" placeholder="" class="input-xlarge" required>
                        <p class="help-block">Kasutajanimi võib sisaldada kõiki tähemärke, vä. tühik</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- Password-->
                     <label class="control-label" for="password">Parool *</label>
                     <div class="controls">
                        <input type="password" id="password" name="password" placeholder="" class="input-xlarge" required>
                        <p class="help-block">Parool peaks olema vähemalt 8 tähemärki</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- Password confirm-->
                     <label class="control-label" for="password">Parool uuesti *</label>
                     <div class="controls">
                        <input type="password" id="passwordconf" name="passwordconf" placeholder="" class="input-xlarge" required>
                        <p class="help-block">Parool uuesti</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- First name -->
                     <label class="control-label" for="email">Eesnimi *</label>
                     <div class="controls">
                        <input type="text" id="firstname" name="firstname" placeholder="" class="input-xlarge" required>
                        <p class="help-block">Teie eesnimi</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- Last name-->
                     <label class="control-label" for="password">Perekonnanimi *</label>
                     <div class="controls">
                        <input type="text" id="lastname" name="lastname" placeholder="" class="input-xlarge" required>
                        <p class="help-block">Teie perekonnanimi</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- Password-->
                     <label class="control-label" for="email">Email *</label>
                     <div class="controls">
                        <input type="email" id="email" name="email" placeholder="" class="input-xlarge" required>
                        <p class="help-block">Toimiv e-maili aadress</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- address-->
                     <label class="control-label" for="address">Aadress</label>
                     <div class="controls">
                        <input type="text" id="address" name="address" placeholder="" class="input-xlarge">
                        <p class="help-block">Aadress formaadis: linn, tänav, majanr.</p>
                     </div>
                  </div>
                  <div class="control-group">
                     <!-- Button -->
                     <div class="controls">
                        <button class="btn btn-success">Registreeri</button>
                     </div>
                  </div>
               </fieldset>
            </form>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <script src="js/customjs.js"></script>
   </body>
</html>
