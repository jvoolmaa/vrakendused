<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div>
      <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="index.php">Orgonize Eesti</a>
      </div>
      <div class="navbar-collapse collapse">
         <ul class="nav navbar-nav navbar-right">
            <li <?php echo $page_title=="Tooted" ? "class='active'" : ""; ?> >
               <a href="products.php">Tooted</a>
            </li>
            <li <?php echo $page_title=="Ostukorv" ? "class='active'" : ""; ?> >
               <a href="cart.php">
               <?php
                  //count products in cart
                  $cookie = isset($_COOKIE['cart_items_cookie']) ? $_COOKIE['cart_items_cookie'] : "";
                  $cookie = stripslashes($cookie);
                  $saved_cart_items = json_decode($cookie, true);
                  $cart_count=count($saved_cart_items);
                  ?>
               Ostukorv <span class="badge" id="comparison-count"><?php echo $cart_count; ?></span>
               </a>
            </li>
            <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>Logi välja</a></li>
         </ul>
      </div>
   </div>
</div>
