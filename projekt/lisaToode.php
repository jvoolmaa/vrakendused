<?php
require("database.php");
if (logged() && isAdmin($_SESSION['id'])) {
    if (isset($_POST['nimi'], $_POST['kirjeldus'], $_POST['hind'], $_POST['kogus'])) {
        addProduct($_POST['nimi'], $_POST['kirjeldus'], $_POST['hind'], $_POST['kogus']);
        header("Refresh:0");
    }
}
?>