<?php
   $page_title="TOOTED";
   include 'layout_head.php';
    
   
   $action = isset($_GET['action']) ? $_GET['action'] : "";
   $name = isset($_GET['name']) ? $_GET['name'] : "";
   $price = isset($_GET['hind']) ? $_GET['hind'] : "";
   $kirjeldus = isset($_GET['kirjeldus']) ? $_GET['kirjeldus'] : "";
    
   // messages
   if($action=='added'){
       echo "<div class='alert alert-info'>";
           echo "<strong>{$name}</strong> lisati ostukorvi!";
       echo "</div>";
   }
    
   else if($action=='exists'){
       echo "<div class='alert alert-info'>";
           echo "<strong>{$name}</strong> on juba ostukorvis!";
       echo "</div>";
   }
    
   // query
   $query = "SELECT id, nimi, kirjeldus, hind FROM jvoolmaa_tooted ORDER BY nimi";
   $stmt = $con->prepare( $query );
   $stmt->execute();
    
   $num = $stmt->rowCount();
    
   if($num>0){
       
       echo "<div><table class='table table-inverse table-responsive table-bordered'>";
    
           echo "<tr>";
               echo "<th class='thead-default'>Toode</th>";
               echo "<th>Kirjeldus</th>";
               echo "<th>Hind (EUR)</th>";
               echo "<th>Valik</th>";
           echo "</tr>";
    
           while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
               extract($row);
    
               //making rows
               echo "<tr>";
                   echo "<td>{$nimi}</td>";
                   echo "<td>{$kirjeldus}</td>";
                   echo "<td>&#128;{$hind}</td>";
                   echo "<td>";
                       echo "<a href='add_to_cart.php?id={$id}&name={$nimi}' class='btn btn-primary'>";
                           echo "<span class='glyphicon glyphicon-shopping-cart'></span>  Lisa ostukorvi";
                       echo "</a>";
                   echo "</td>";
               echo "</tr>";
           }
    
       echo "</table></div>";
   }
    
   //no products in db
   else {
       echo "Tooteid ple nh.";
   }
    
   include 'layout_foot.php';
   ?>
